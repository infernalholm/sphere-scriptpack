## Summary

<!-- Summarize the bug encountered concisely -->

## Steps to reproduce

<!-- How one can reproduce the issue - this is very important -->

1. 
2. 
3. 

## What is the current bug behavior?

<!-- What actually happens -->

## What is the expected correct behavior?

<!-- What you should see instead -->

## Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code as it's very hard to read otherwise. -->

## Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem -->

## Your Environment

<!--- Include as many relevant details about the environment you use -->

- Client version (e.g.: Client 5.0.9.1):
- Server version (e.g.: Sphere 0.56b):
- Operating System and version (e.g.: Windows 7):

/label ~bug ~needs-investigation
/cc @project-manager
/assign @qa-tester

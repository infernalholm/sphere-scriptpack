## Difficulty

<!-- Delete what doesn't apply -->

- Easy <!-- Can be done alone -->
- Medium <!-- Can be done alone, but requires to have >800 of skill -->
- Hard <!-- Should not be possible to be done alone -->
- Very hard <!-- Requires more than one player, with planning and sync communication-->

## Regularity

<!-- Delete what doesn't apply -->

- Daily quest
- Seasonal quest <!-- Christmas, Halloween... -->
- Staff

## Category

<!-- Delete what doesn't apply -->

- Turn-in <!-- bring the NPC certain items -->
- Chain quest <!-- part of a chain must be unlocked by first completing previous quests in the chain -->
- Kill quest <!-- Kill quests involve killing a destined amount of the same creature. -->
- Collection <!-- Collecting objects from an area or from monster drops. -->
- Timed Quests <!-- Quests that make you go to one place in a set amount of time -->
- Profession Specific <-- Quests that are available for those trained in certain professions.(Stealth, Blacksmith) -->

## Storyline

<!-- Describe what's happening in the world that caused the need for this quest -->

## Places

<!--
Describe in the map where the quest takes place, if possible place screen-shots
of the exact locations.
-->

## Actions required by the player

<!-- Describe the steps the player must do to complete the quest -->

1.
2.
3.

## NPC

<!-- List required NPC characters that need to be created for the quest-->

-
-

## Items

<!-- List required items that need to be created for the quest-->

-
-

## Rewards

<!-- List the rewards for the players that complete the quest-->

-
-

/label ~Quest
/cc @project-manager
/assign @qa-tester

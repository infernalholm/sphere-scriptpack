# Sphere ScriptPack

![Infernal Holm](https://gitlab.com/infernalholm/design-guidelines/raw/master/png/infernal-holm-title.png)

---

SphereServer 0.56b Scripts.

They were originally created and maintained by the developers of MPZ, BFUO and
TUP.


<!-- vim-markdown-toc GitLab -->

* [How to run](#how-to-run)

<!-- vim-markdown-toc -->

## How to run

```bash
docker run \
  --interactive \
  --tty \
  -p 2593:2593 \
  -e EXTERNAL_IP="your-cluster-ip" \
  -e EXTERNAL_PORT="your-node-port" \
  registry.gitlab.com/infernalholm/sphere-scriptpack:stable
```

This will start a docker container with the correct sphere server and the
latest scripts in master.



#!/usr/bin/env bash
#
# This is proxy file, it receives a command, and before executing that command:
# 1. Read environment variables. 
# 2. If variables of interest are set, then write an .ini file with those
# variables.

set -euo pipefail
IFS=$'\n\t'


FILENAME="externalvalues.ini";

log_error() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: ${@}" >&2;
}

log_info() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: ${@}";
}

create_file(){
  if [[ -f $FILENAME ]]; then
    log_error "File: ${FILENAME} already exists. Aborting script."
    exit 1
  else
    touch ${FILENAME};
    log_info "Created file: ${FILENAME}"
  fi
}

write_env(){
  local prop_name="${1}";
  local env_value="${2}";

  if [ ! -z "${env_value}" ]; then  
    echo "${prop_name}=${env_value}" >> "${FILENAME}"
    log_info "Wrote environment variable for ${1}";
  fi
}

execute_command(){
  if [[ ! -z $@ ]]; then
    log_info "Detected command as argument. Executing: \`$@\`";
    exec -c "$@";
  else 
    log_info "Could not find command to execute. Skipping.";
  fi
}

create_file "${FILENAME}";
write_env "external_ip" "$(printenv SPHERE_EXTERNAL_IP)"
write_env "external_port" "$(printenv SPHERE_EXTERNAL_PORT)"
write_env "account_acceptance_level" "$(printenv SPHERE_ACCOUNT_ACCEPTANCE_LEVEL)"
write_env "admin_account" "$(printenv SPHERE_ADMIN_ACCOUNT)"
execute_command "$@"

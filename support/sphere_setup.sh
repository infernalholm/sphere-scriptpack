#!/usr/bin/env bash

# variables
SPHERE_URL="https://forum.spherecommunity.net/sshare.php?downproj=48" # SpherServer 0.56b
MYSQLLIB_URL="http://launchpadlibrarian.net/94563300/libmysqlclient16_5.1.58-1ubuntu5_i386.deb"
MYSQL_PASSWORD="Password1"
SPHERE_HOME="/vagrant"
TEMP="/var/tmp"

echo "Update and install basic packages"
apt-get update --fix-missing
apt-get install -y tmux curl htop vim

# install mysql if a password is set
echo "Install MySQL if a password is set"
if [[ $MYSQL_PASSWORD ]]; then

  echo "mysql-server-5.5 mysql-server/root_password password $MYSQL_PASSWORD" | debconf-set-selections
  echo "mysql-server-5.5 mysql-server/root_password_again password $MYSQL_PASSWORD" | debconf-set-selections
  apt-get -y install mysql-common 

fi

echo "Install sphere dependencies"
curl -L -s $MYSQLLIB_URL > $TEMP/libmysqlclient.deb
dpkg -i $TEMP/libmysqlclient.deb


echo "Instal sphere if no sphere is present"
if [[ ! -f $SPHERE_HOME/spheresvr ]]; then
  curl -L -s $SPHERE_URL > $TEMP/sphere.tar.gz
  mkdir -p $TEMP/sphere
  tar -xzf $TEMP/sphere.tar.gz -C $TEMP/sphere
  false | cp -i $TEMP/sphere/* $SPHERE_HOME > /dev/null 2>&1
fi

echo "Setup sphere user if it doesn't exist already"
id -u sphere &>/dev/null || useradd sphere

echo "Create files if they are missing"
[ -f $SPHERE_HOME/accounts/sphereaccu.scp ] || echo "[EOF]" > $SPHERE_HOME/accounts/sphereaccu.scp
[ -f $SPHERE_HOME/save/spherechars.scp ] || echo "[EOF]" > $SPHERE_HOME/save/spherechars.scp
[ -f $SPHERE_HOME/save/spheredata.scp ] || echo "[EOF]" > $SPHERE_HOME/save/spheredata.scp
[ -f $SPHERE_HOME/save/spheremultis.scp ] || echo "[EOF]" > $SPHERE_HOME/save/spheremultis.scp
[ -f $SPHERE_HOME/save/spherestatics.scp ] || echo "[EOF]" > $SPHERE_HOME/save/spherestatics.scp
[ -f $SPHERE_HOME/save/sphereworld.scp ] || echo "[EOF]" > $SPHERE_HOME/save/sphereworld.scp

echo "Set permissions"
chown -R sphere:sphere $SPHERE_HOME/*
find $SPHERE_HOME -type d | xargs chmod 0700
find $SPHERE_HOME -type f | xargs chmod 0600

[ -f $SPHERE_HOME/spheresvr ] && chmod 4700 $SPHERE_HOME/spheresvr
[ -f $SPHERE_HOME/sphereNightly ] && chmod 4700 $SPHERE_HOME/sphereNightly
[ -f $SPHERE_HOME/sphereNightlyDebug ] && chmod 4700 $SPHERE_HOME/sphereNightlyDebug

exit 0;

FROM registry.gitlab.com/infernalholm/sphere-dockerimage:stable

WORKDIR /sphere

COPY accounts ./accounts
COPY mul ./mul
COPY save ./save
COPY scripts ./scripts
COPY sphere.ini .
COPY ./support/boot.sh .

# Normalize UID/GID
# RUN chown -R root:root .

ENTRYPOINT ["./boot.sh"]
CMD ["/sphere/spheresvr"]
